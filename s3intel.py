#!/usr/bin/env python

import boto3
import statistics
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--profile", help="~/.aws/config profile to use", required=False, default=None)
parser.add_argument("--region", help="AWS region to use", required=True)
parser.add_argument("--buckets", help="buckets to target", required=True,  nargs='+')
parser.add_argument("--prefix", help="prefix", nargs="?", default="")
parser.add_argument("--group", help="position of extension, for example --group=2 would catch .txt from foo.txt.aaa", default=1, nargs="?")
args = parser.parse_args()

profile = args.profile
region = args.region 
bucket_names = args.buckets
prefix = args.prefix
group = int(args.group)

session = boto3.Session(profile_name=profile)
s3_resource = session.resource('s3',region_name=region)

file_type_size_hashmap = {}

for bucket in bucket_names:
	cur_bucket = s3_resource.Bucket(bucket)
	
	if prefix is None:
	    objects = cur_bucket.objects.all()
	else:
	    objects = cur_bucket.objects.filter(Prefix=prefix)
	for obj in objects:

		if obj.size != 0:

			path_parts = obj.key.split('/')
			file_name = path_parts[-1]

			if '.' in file_name:
				file_name_array = file_name.split('.')
				file_type = str(file_name_array[-group]).lower()
			else:
				file_type = 'typeless'

			if file_type not in file_type_size_hashmap:
				file_type_size_hashmap[file_type] = []
				file_type_size_hashmap[file_type].append(obj.size)
			else:
				file_type_size_hashmap[file_type].append(obj.size)
		else:
			continue

def bytes_2_human_readable(number_of_bytes):
    if number_of_bytes < 0:
        raise ValueError("!!! numberOfBytes can't be smaller than 0 !!!")

    step_to_greater_unit = 1024.

    number_of_bytes = float(number_of_bytes)
    unit = 'bytes'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'KiB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'MiB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'GiB'

    if (number_of_bytes / step_to_greater_unit) >= 1:
        number_of_bytes /= step_to_greater_unit
        unit = 'TiB'

    precision = 1
    number_of_bytes = round(number_of_bytes, precision)

    return str(number_of_bytes) + ' ' + unit

def compute(file_size_array):
	final_sum = sum(file_size_array)
	final_min = min(file_size_array)
	final_max = max(file_size_array)
	final_median = statistics.median(file_size_array)
	final_mean = statistics.mean(file_size_array)

	# Data set size
	print('Total Size:', final_sum, ' / ', bytes_2_human_readable(final_sum))

	# File Count
	print('File Count:', len(file_size_array))

	# min
	print('min:', final_min, ' / ', bytes_2_human_readable(final_min))

	# max
	print('max:', final_max, ' / ', bytes_2_human_readable(final_max))

	# median
	print('median:', final_median, ' / ', bytes_2_human_readable(final_median))

	# average
	print('avg:', final_mean, ' / ', bytes_2_human_readable(final_mean))

	small_files = 0
	kb_files = 0
	mb_files = 0
	gb_files = 0

	for num in file_size_array:
		if num < 1024:
			small_files += 1
		elif num >= 1024 and num <= 1048576:
			kb_files += 1
		elif num > 1048576 and num <= 1073741824:
			mb_files += 1
		elif num > 1073741824 and num <= 1099511627776:
			gb_files += 1
		else:
			print('num not in range')

	# ranges 
	print('Files under 1 KiB:', small_files)
	print('1 KiB <= Files < 1 MiB:', kb_files)
	print('1 MiB <= Files < 1 GiB:', mb_files)
	print('Files over 1 GiB:', gb_files)
	return

def stats(file_info_dict):
	size_accumulator = []

	print('=======================================================')
	print('File Type Results:')
	print('=======================================================')
	print('File Types:', file_info_dict.keys())

	for  file_type_key, size_array in file_info_dict.items():

		size_accumulator = size_accumulator + size_array

		print('=======================================================')
		print('File Type:', file_type_key)

		compute(size_array)

	print('=======================================================')
	print('Full Data Set Results:')
	print('=======================================================')

	compute(size_accumulator)
	return

stats(file_type_size_hashmap)

('=======================================================')
